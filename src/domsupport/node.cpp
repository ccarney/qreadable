/**
 * SPDX-FileCopyrightText: 2022 Connor Carney <hello@connorcarney.com>
 * SPDX-License-Identifier: Apache-2.0
 */
#include "domsupport_p.h"
#include <gumbo/gumbo.h>
using namespace QReadable;
using namespace QReadable::DomSupport;

void Node::clear() {
  while (auto *last = lastChild()) {
    removeChild(last);
  }
}

DomSupport::Node *DomSupport::Node::firstChild() const {
  return m_childNodes.value(0, nullptr);
}

DomSupport::Element *DomSupport::Node::firstElementChild() const {
  return m_children.value(0, nullptr);
}

DomSupport::Node *DomSupport::Node::lastChild() const {
  if (m_childNodes.isEmpty()) {
    return nullptr;
  }
  return m_childNodes.last();
}

DomSupport::Element *DomSupport::Node::lastElementChild() const {
  if (m_children.isEmpty()) {
    return nullptr;
  }
  return m_children.last();
}

Document *Node::ownerDocument() const {
  if (m_ownerDocument) {
    return m_ownerDocument;
  }
  for (Node *eachAncestor = m_parentNode; eachAncestor;
       eachAncestor = eachAncestor->m_parentNode) {
    if (auto *doc = dynamic_cast<Document *>(eachAncestor)) {
      return doc;
    } else if (auto *doc = eachAncestor->m_ownerDocument) {
      return doc;
    }
  }
  return nullptr;
}

void Node::setOwnerDocument(Document *owner) { m_ownerDocument = owner; }

void Node::walkNextElement(Element *&el) {
  if (Element *fec = el->firstElementChild()) {
    el = fec;
  } else if (Element *nes = el->m_nextElementSibling) {
    el = nes;
  } else {
    // find the first parent that has a next element
    while (el) {
      if (auto *parentElement = dynamic_cast<Element *>(el->m_parentNode); parentElement && parentElement!=this) {
        if (Element *parentNext = parentElement->m_nextElementSibling) {
          el = parentNext;
          return;
        }
        el = parentElement;
      } else {
        el = nullptr;
      }
    }
  }
}

QList<Element *> Node::getElementsByTagName(const QString &tag, int max_elems) {
  int n{0};
  if (m_children.isEmpty()) {
    return {};
  }
  bool isGetAll = (tag == "*");
  QString tagUpper = tag.toUpper();
  GumboTag gumboTag = gumbo_tag_enum(tagUpper.toUtf8());
  QList<Element *> result;
  Element *candidate = m_children.first();
  while (candidate) {
    if (isGetAll || candidate->gumboTag() == gumboTag) {
      result.append(candidate);
      ++n;
      if (n > max_elems) {
        break;
      }
    }
    walkNextElement(candidate);
  }
  return result;
}

QList<Element *> Node::getElementsByTagName(const QString &tag) {
  return getElementsByTagName(tag, INT_MAX);
}

void DomSupport::Node::appendChild(Node *child) {
  child->setParent(this);
  if (child->m_parentNode) {
    child->m_parentNode->removeChild(child);
  }
  Node *last = lastChild();
  if (last) {
    last->m_nextSibling = child;
  }
  child->m_previousSibling = last;
  if (auto *childElement = dynamic_cast<Element *>(child)) {
    childElement->m_previousElementSibling = lastElementChild();
    m_children.push_back(childElement);
    if (Element *prev = childElement->m_previousElementSibling) {
      prev->m_nextElementSibling = childElement;
    }
  }
  m_childNodes.push_back(child);
  child->m_parentNode = this;
}

Node *Node::removeChild(Node *child) {
  int childIndex = m_childNodes.indexOf(child);
  if (childIndex < 0) {
    return nullptr; // TODO should throw
  }
  child->m_parentNode = nullptr;
  Node *prev = child->m_previousSibling;
  Node *next = child->m_nextSibling;
  if (prev) {
    prev->m_nextSibling = next;
  }
  if (next) {
    next->m_previousSibling = prev;
  }

  if (auto childElement = dynamic_cast<Element *>(child)) {
    Element *prevElement = childElement->m_previousElementSibling;
    Element *nextElement = childElement->m_nextElementSibling;
    if (prevElement) {
      prevElement->m_nextElementSibling = nextElement;
    }
    if (nextElement) {
      nextElement->m_previousElementSibling = prevElement;
    }
    childElement->m_previousElementSibling =
        childElement->m_nextElementSibling = nullptr;
    m_children.removeOne(childElement);
  }
  child->m_previousSibling = child->m_nextSibling = nullptr;
  m_childNodes.removeAt(childIndex);
  return child;
}

static void updateElementLinks(Element *newElement, Element *oldElement) {
  newElement->m_previousElementSibling = oldElement->m_previousElementSibling;
  newElement->m_nextElementSibling = oldElement->m_nextElementSibling;
  if (auto *prev = newElement->m_previousElementSibling) {
    prev->m_nextElementSibling = newElement;
  }
  if (auto *next = newElement->m_nextElementSibling) {
    next->m_previousElementSibling = newElement;
  }
}

static void updateElementLinks(Element *newElement, Node *oldNode) {
  newElement->m_previousElementSibling = nullptr;
  for (auto *eachNode = oldNode->m_previousSibling; eachNode;
       eachNode = eachNode->m_previousSibling) {
    if (auto *eachElement = dynamic_cast<Element *>(eachNode)) {
      newElement->m_previousElementSibling = eachElement;
      break;
    }
  }
  if (auto *prev = newElement->m_previousElementSibling) {
    newElement->m_nextElementSibling = prev->m_nextElementSibling;
  } else {
    newElement->m_nextElementSibling = nullptr;
    for (auto *eachNode = oldNode->m_nextSibling; eachNode;
         eachNode = eachNode->m_nextSibling) {
      if (auto *eachElement = dynamic_cast<Element *>(eachNode)) {
        newElement->m_nextElementSibling = eachElement;
        break;
      }
    }
  }

  if (auto *prev = newElement->m_previousElementSibling) {
    prev->m_nextElementSibling = newElement;
  }
  if (auto *next = newElement->m_nextElementSibling) {
    next->m_previousElementSibling = newElement;
  }
}

static void updateElementLinks(Node * /* newNode */, Element *oldElement) {
  if (auto *prev = oldElement->m_previousElementSibling) {
    prev->m_nextElementSibling = oldElement->m_nextElementSibling;
  }
  if (auto *next = oldElement->m_nextElementSibling) {
    next->m_previousElementSibling = oldElement->m_previousElementSibling;
  }
}

Node *Node::replaceChild(Node *newNode, Node *oldNode) {
  int childIndex = m_childNodes.indexOf(oldNode);
  if (childIndex < 0) {
    return nullptr; // TODO should throw
  }
  newNode->setParent(this);
  if (Node *oldParent = newNode->m_parentNode) {
    oldParent->removeChild(newNode);
  }
  m_childNodes[childIndex] = newNode;
  newNode->m_nextSibling = oldNode->m_nextSibling;
  newNode->m_previousSibling = oldNode->m_previousSibling;
  if (Node *next = newNode->m_nextSibling) {
    next->m_previousSibling = newNode;
  }
  if (Node *prev = newNode->m_previousSibling) {
    prev->m_nextSibling = newNode;
  }
  newNode->m_parentNode = this;

  if (auto *newElement = dynamic_cast<Element *>(newNode)) {
    if (auto *oldElement = dynamic_cast<Element *>(oldNode)) {
      // replace an element with an element
      updateElementLinks(newElement, oldElement);
      m_children.replace(m_children.indexOf(oldElement), newElement);
    } else {
      // replace a non-element with an element
      updateElementLinks(newElement, oldNode);
      if (auto *next = newElement->m_nextElementSibling) {
        m_children.insert(m_children.indexOf(next), newElement);
      } else {
        m_children.push_back(newElement);
      }
    }
  } else if (auto *oldElement = dynamic_cast<Element *>(oldNode)) {
    // replace an element with a non-element
    updateElementLinks(newNode, oldElement);
    m_children.removeOne(oldElement);
  }

  oldNode->m_parentNode = nullptr;
  oldNode->m_previousSibling = nullptr;
  oldNode->m_nextSibling = nullptr;
  if (auto *oldElement = dynamic_cast<Element *>(oldNode)) {
    oldElement->m_previousElementSibling = nullptr;
    oldElement->m_nextElementSibling = nullptr;
  }
  return oldNode;
}
