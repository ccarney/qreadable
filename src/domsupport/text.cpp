/**
 * SPDX-FileCopyrightText: 2022 Connor Carney <hello@connorcarney.com>
 * SPDX-License-Identifier: Apache-2.0
 */
#include "dombuilder_p.h"
#include "domsupport_p.h"
#include <gumbo/gumbo.h>
using namespace QReadable;
using namespace QReadable::DomSupport;

QString Text::innerHTML() {
  if (m_html.isNull()) {
    m_html = m_text.toHtmlEscaped();
  }
  return m_html;
}

void Text::setInnerHTML(const QString &html) {
  m_html = html;
  m_text.clear();
}

QString Text::textContent() {
  if (m_text.isNull()) {
    if (m_html.isEmpty()) {
      m_text = "";
    } else {
      QScopedPointer<Element> body(new Element("body"));
      DomBuilder builder(m_html, GUMBO_TAG_BODY);
      builder.buildIntoNode(body.data());
      m_text = body->textContent();
    }
  }
  return m_text;
}

void Text::setTextContent(const QString &text) {
  m_text = text;
  m_html.clear();
}

void Text::appendTextContent(const QString &text, const QString &html) {
  m_text += text;
  m_html += html;
}

void Text::serialize(QStringList &fragments, bool textOnly) {
  if (textOnly) {
    fragments << textContent();
  } else {
    fragments << innerHTML();
  }
}
