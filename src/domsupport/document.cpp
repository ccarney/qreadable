/**
 * SPDX-FileCopyrightText: 2022 Connor Carney <hello@connorcarney.com>
 * SPDX-License-Identifier: Apache-2.0
 */
#include "domsupport_p.h"
using namespace QReadable;
using namespace QReadable::DomSupport;

Document::Document(const QString &url) : m_url(url), m_baseURI(url) {}

void Document::serialize(QStringList &fragments, bool textOnly) {
  for (auto *eachChild : std::as_const(m_childNodes)) {
    eachChild->serialize(fragments, textOnly);
  }
}

Element *Document::documentElement() {
  return m_children.length() > 0 ? m_children.first() : nullptr;
}

QString Document::title() {
  QList<Element *> match = getElementsByTagName("title", 1);
  if (match.isEmpty()) {
    return QString();
  }
  return match.first()->textContent();
}

Element *Document::body() {
  QList<Element *> match = getElementsByTagName("body");
  if (match.isEmpty()) {
    return nullptr;
  }
  return match.first();
}

Element *Document::head() {
  QList<Element *> match = getElementsByTagName("head");
  if (match.isEmpty()) {
    return nullptr;
  }
  return match.first();
}

Element *Document::getElementById(const QString &id) {
  if (m_children.isEmpty()) {
    return {};
  }
  Element *candidate = m_children.first();
  while (candidate) {
    if (candidate->id() == id) {
      return candidate;
    }
    walkNextElement(candidate);
  }
  return nullptr;
}

Element *Document::createElement(const QString &tag) {
  auto *result = new Element(tag);
  result->setOwnerDocument(this);
  return result;
}

Text *Document::createTextNode(const QString &text) {
  auto *result = new Text();
  result->setTextContent(text);
  result->setOwnerDocument(this);
  return result;
}
