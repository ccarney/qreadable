/**
 * SPDX-FileCopyrightText: 2022 Connor Carney <hello@connorcarney.com>
 * SPDX-License-Identifier: Apache-2.0
 */
#include "domsupport_p.h"
using namespace QReadable;
using namespace QReadable::DomSupport;

Attribute::Attribute(const QString &name, const QString &value)
    : m_name(name), m_value(value) {}

void Attribute::serialize(QStringList &fragments, bool textOnly) {
  if (textOnly) {
    return;
  }
  fragments << " " + m_name + "=\"" + getEncodedValue() + "\"";
}

QString Attribute::getEncodedValue() const { return m_value.toHtmlEscaped(); }
