/**
 * SPDX-FileCopyrightText: 2022 Connor Carney <hello@connorcarney.com>
 * SPDX-License-Identifier: Apache-2.0
 */
#include "domsupport_p.h"
using namespace QReadable;
using namespace QReadable::DomSupport;

Style::Style(Element *node) : QObject(node), m_element(node) {}

QString Style::getStyle(const QString &styleName) const {
  QString styleAttr = m_element->getAttribute("style");
  if (styleAttr.isEmpty()) {
    return QString();
  }
  const QStringList styles = styleAttr.split(";");
  for (const QString &css : styles) {
    QStringList splitCss = css.split(":");
    if (splitCss.length() != 2) {
      continue;
    }
    QString name = splitCss.first().trimmed();
    if (name == styleName) {
      return splitCss.last().trimmed();
    }
  }
  return QString();
}

void Style::setStyle(const QString &styleName, const QString &styleValue) {
  QString cssText = m_element->getAttribute("style");
  const QString::iterator begin = cssText.begin();
  const QString::iterator end = cssText.end();
  QString::iterator index = begin;
  while (index < end) {
    QString::iterator declEnd = std::find(index, end, ';');
    QString::iterator colon = std::find(index, declEnd, ':');
    if (colon != declEnd) {
        QStringView cssDecl(index, colon);
        if (cssDecl.trimmed() == styleName) {
          // found the entry for styleName, replace it
          QStringView prefix = QStringView(begin, index);
          QStringView suffix =
              declEnd < end ? QStringView(declEnd + 1, end) : QStringView();
          cssText = QLatin1String("%1%2").arg(prefix, suffix);
          break;
        }
    }
    index = declEnd + 1;
  }

  // didn't find
  if (!cssText.endsWith(';')) {
    cssText += ';';
  }
  cssText = QLatin1String("%1 %2: %3;").arg(cssText, styleName, styleValue);
  m_element->setAttribute("style", cssText);
}

QString Style::display() const { return getStyle("display"); }

void Style::setDisplay(const QString &newDisplay) {
  setStyle("display", newDisplay);
}
