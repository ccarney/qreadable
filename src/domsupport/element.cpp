/**
 * SPDX-FileCopyrightText: 2022 Connor Carney <hello@connorcarney.com>
 * SPDX-License-Identifier: Apache-2.0
 */
#include "dombuilder_p.h"
#include "domsupport_p.h"
#include <gumbo/gumbo.h>
using namespace QReadable;
using namespace QReadable::DomSupport;

Element::Element(const QString &tag)
    : m_tagName(tag.toUpper()), m_style(new Style(this)) {}

Element::Element(int gumboTag)
    : Element(gumbo_normalized_tagname(static_cast<GumboTag>(gumboTag))) {
  m_gumboTag = gumboTag;
}

void Element::serialize(QStringList &fragments, bool textOnly) {
  if (textOnly) {
    serializeChildren(fragments, true);
    return;
  }
  QString name = localName();
  fragments << "<" << name;
  for (auto *eachAttr : std::as_const(m_attributes)) {
    eachAttr->serialize(fragments);
  }

  if (m_childNodes.isEmpty()) {
    fragments << "/>";
  } else {
    fragments << ">";
    serializeChildren(fragments, textOnly);
    fragments << "</" << name << ">";
  }
}

QString Element::innerHTML() {
  QStringList fragments;
  serializeChildren(fragments, false);
  return fragments.join("");
}

void Element::setInnerHTML(const QString &html) {
  clear();
  if (!html.isEmpty()) {
    DomBuilder builder(html, static_cast<GumboTag>(gumboTag()));
    builder.buildIntoNode(this);
  }
}

QString Element::textContent() {
  QStringList fragments;
  serializeChildren(fragments, true);
  return fragments.join("");
}

void Element::setTextContent(const QString &text) {
  clear();
  Text *textNode = new Text();
  textNode->setTextContent(text);
  appendChild(textNode);
}

QString Element::className() const { return getAttribute("class"); }

void Element::setClassName(const QString &newClassName) {
  setAttribute("class", newClassName);
}

QString Element::id() const { return getAttribute("id"); }

void Element::setId(const QString &newId) { setAttribute("id", newId); }

QString Element::href() const { return getAttribute("href"); }

void Element::setHref(const QString &newHref) { setAttribute("href", newHref); }

QString Element::src() const { return getAttribute("src"); }

void Element::setSrc(const QString &newSrc) { setAttribute("src", newSrc); }

QString Element::srcset() const { return getAttribute("srcset"); }

void Element::setSrcset(const QString &newSrcset) {
  setAttribute("srcset", newSrcset);
}

QString Element::tagName() const { return m_tagName; }

QString Element::localName() { return m_tagName.toLower(); }

int Element::gumboTag() const {
  if (m_gumboTag < 0) {
    m_gumboTag = gumbo_tag_enum(m_tagName.toUtf8());
  }
  return m_gumboTag;
}

QString Element::getAttribute(const QString &name) const {
  for (Attribute *eachAttr : std::as_const(m_attributes)) {
    if (eachAttr->m_name == name) {
      return eachAttr->m_value;
    }
  }
  return QString();
}

void Element::setAttribute(const QString &name, const QString &value) {
  for (Attribute *eachAttr : std::as_const(m_attributes)) {
    if (eachAttr->m_name == name) {
      eachAttr->m_value = value;
      return;
    }
  }
  auto *newAttr = new Attribute(name, value);
  newAttr->setParent(this);
  m_attributes.append(newAttr);
}

void Element::removeAttribute(const QString &name) {
  auto it =
      std::find_if(m_attributes.begin(), m_attributes.end(),
                   [&name](Attribute *&attr) { return name == attr->m_name; });
  if (it == m_attributes.end()) {
    return;
  }
  m_attributes.erase(it);
}

bool Element::hasAttribute(const QString &name) {
  return std::find_if(m_attributes.begin(), m_attributes.end(),
                      [&name](Attribute *&attr) {
                        return name == attr->m_name;
                      }) != m_attributes.end();
}

void Element::serializeChildren(QStringList &fragments, bool textOnly) {
  for (auto *eachChild : std::as_const(m_childNodes)) {
    eachChild->serialize(fragments, textOnly);
  }
}
