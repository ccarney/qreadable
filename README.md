# QReadable
A library and command line tool for running Readability.js with Qt's Javascript engine

## Overview
QReadable embeds Readability.js as a Qt resource and runs it with Qt's JS engine.  It uses the gumbo parser and a custom QObject implementation of the DOM to provide a document tree for readability to work on.

## Using QReadable
### As a library

```c++
#include <qreadable/readable.h>
...
QString rawHtml = "<html>...</html>";
QUrl url("https://example.com/path/to/some.html"); // for resolving relative URLs
QReadable::Readable readable;
QString readableText = readable.parse(rawHtml, url)
```

Every `QReadable::Readable` object loads its own copy of Readability.js, so it is recommended to re-use these objects whenever possible. *However*, these objects are not thread-safe, and can only be safely used from the thread they were created on. Multi-threaded applications will need to create a separate instance for each thread.

### From the command line
QReadable installs a basic `qreadable` command line tool:
```sh
qreadable https://example.com/path/to/some.html
```
## Building from Source
QReadable is build using cmake:
```sh
cmake . -Bbuild
cmake --build build
cmake --install build
```
### Using a different version of Readability
The `3rdparty/readability` subtree includes a copy of the most recent version of [Mozilla's readability](https://github.com/mozilla/readability) that has been tested and found to work, however you should be able to replace it with any other version of readability provided that it uses the same file layout.

> **Note**
> QReadable implements *only* the subset of the DOM necessary to support Readability.js. If you encounter a new/alternative version of readability that depends on an unimplemented DOM feature, please open an issue.